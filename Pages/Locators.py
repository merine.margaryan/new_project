from .Base import BaseScreen
from Pages.Strings import strings

class Login():
    def __init__(self, driver):
        self.driver= driver

        self.username_textbox_name="//input[@type='email']"
        self.passward_textbox_name="pass"
        self.login_button_id="u_0_4"

    def enter_username(self, username):
        self.driver.find_element_by_xpath(self.username_textbox_name).send_keys(username)



    def enter_password(self, password):
        self.driver.find_element_by_name(self.passward_textbox_name).send_keys(password)

    def click_login(self):
        self.driver.find_element_by_id(self.login_button_id).click()
